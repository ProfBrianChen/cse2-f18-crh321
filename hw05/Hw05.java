// Chiara Hubrich 10/8/18
// CSE 02 HW 5
// Uses the scanner class to ask the user how many hands they want generated, then generates 5 random cards for each hand using while loops
// and then calculating the probabilities of 4 of a kind, 3 of a kind, 2 pair, and 1 pair

// import scanner class
import java.util.Scanner;
// define a class
public class Hw05{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    
    int numHands = 0; // the number of hands the user will want
    String wrongVal = ""; // used to store values of incorrect inputs (such as strings)
    String enterVal = ""; // used to prevent the enter in the scanner input from being considered an input
    boolean testVal = false; // boolean used for hasNextInt to check if the user input is an integer
    
    // while loop to allow the user to input value and checks if the value is an integer
    while (numHands <= 0){
      System.out.println("Please input the amount of hands you want to be generated: ");
      testVal = myScanner.hasNextInt();
      
      if (!testVal){
        wrongVal = myScanner.nextLine();
        System.out.println("Incorrect input."); // outputs if the user inputs something incorrect, like a string
        continue; // restarts the while loop so the user can input a new value
      }
      
      numHands = myScanner.nextInt(); // assigns the user input to a variable after being checked as an integer
      enterVal = myScanner.nextLine(); // prevents the enter the user inputs in the scanner from impacting the integer input
      
    }
    
    int max = 52; // maximum for random generator
    int min = 1; // minimum for random generator
    int counter = 1; // counter for counting up to numHands
    int cardCounter = 1; // counter for counting up to 5 for running while statement for each of the 5 cards in a hand
    int randomNum = 0; // variable for the random number generated for a card
    int card1 = 0, card2 = 0, card3 = 0, card4 = 0, card5 = 0; // the values of each of the 5 cards
    int fourkind = 0, threekind = 0, twopair = 0, onepair = 0; // number of each poker hands found
    
    // while loop for assigning the card values
    while (counter <= numHands){ // limits to number of hands input by user
      while (cardCounter <= 5){ // limits to the 5 cards used
        randomNum = (int) (Math.random()*((max - min) + 1) + min);
        
        // checks that the random number is not equal to any other card value
        if (randomNum == card1){
          continue;
        }
        else if (randomNum == card2){
          continue;
        }
        else if (randomNum == card3){
          continue;
        }
        else if (randomNum == card4){
          continue;
        }
        else if (randomNum == card5){
          continue;
        }
        
        // assigns the random number to the card value
        switch(cardCounter){
          case 1: card1 = randomNum;
            break;
          case 2: card2 = randomNum;
            break;
          case 3: card3 = randomNum;
            break;
          case 4: card4 = randomNum;
            break;
          case 5: card5 = randomNum;
            break;
          default:
            break;
        
        }
      cardCounter++; // increments card counter for next card value to be assigned
      }
    
    // uses modulus operator to assign each value to a number 1-13
    card1 = (card1 - 1) % 13 + 1;
    card2 = (card2 - 1) % 13 + 1;
    card3 = (card3 - 1) % 13 + 1;
    card4 = (card4 - 1) % 13 + 1;
    card5 = (card5 - 1) % 13 + 1;
    
    // the variables for the counters for each number of cards that are equal (in mod 13)
    int n1 = 0;
    int n2 = 0;
    int n3 = 0;
    int n4 = 0;
    
    // increases counters every time a set of cards in equal
    if (card1 == card2){
      n1++;
    }
    if (card1 == card3){
      n1++;
    }
    if (card1 == card4){
      n1++;
    }
    if (card1 == card5){
      n1++;
    }
    if (card2 == card3){
      n2++;
    }
    if (card2 == card4){
      n2++;
    }
    if (card2 == card5){
      n2++;
    }
    if (card3 == card4){
      n3++;
    }
    if (card3 == card5){
      n3++;
    }
    if (card4 == card5){
      n4++;
    }
    
    // assigns the number of times a card is equal to other cards to that specific poker hand count
    if ((n1 == 3) || (n2 == 3)){
        fourkind++;
    }
    else if ((n1 == 2) || (n2 == 2) || (n3 == 2)){
        threekind++;
    }
    else if ((n1 + n2 + n3 + n4) > 1){
        twopair++;
    }
    else if ((n1 + n2 + n3 + n4) == 1){
        onepair++;
    }
    
    // reinitalizes the values for the next hand
    n1 = 0;
    n2 = 0;
    n3 = 0;
    n4 = 0;
    cardCounter = 1;
    counter++; // increments counter for the number of hands
    }
    
    // calculates probability of each poker hand
    double prob4Kind = ((double) fourkind / numHands);
    double prob3Kind = ((double) threekind / numHands);
    double prob2Pair = ((double) twopair / numHands);
    double prob1Pair = ((double) onepair / numHands);

    // prints the number of loops and the probabilities with 3 decimal places
    System.out.println("The number of loops: " + numHands);
    System.out.printf("The probability of a four-of-a-kind: %.3f \n", prob4Kind);
    System.out.printf("The probability of a three-of-a-kind: %.3f \n", prob3Kind);
    System.out.printf("The probability of a two-pair: %.3f \n", prob2Pair);
    System.out.printf("The probability of a one-pair: %.3f \n", prob1Pair);
    
  } // end of main
} // end of class