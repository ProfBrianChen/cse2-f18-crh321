// Chiara Hubrich 10/5/18
// CSE 02 User Input Lab 5
// Uses the scanner class to get inputs from the user about the course they are taking, and uses loops to prevent the wrong type
// of information being entered (such as a string when it should be an int)

// import scanner class
import java.util.Scanner;
// define a class
public class userInput{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    
    int courseNum = 0; // the course number
    int meetingNum = 0; // the number of meetings for the course per week
    int timeStartHour = 0; // the hour the class starts
    int timeStartMin = 0; // the minute the class starts
    int studentNum = 0; // the numer of students in the class
    String instructorName = " "; // the name of the instructor
    String deptName = " "; // the name of the department
    
    String wrongVal = ""; // holds any values the user inputs that are not of the right type
    String enterVal = ""; // holds the value of the enter input when a user types in scanner

    boolean check = false; // the boolean value to check for the correct type in hasNextInt

    // while statement to check the course number input
    while (courseNum <= 0){
      System.out.println("Input the course number: ");
      check = myScanner.hasNextInt();
      if (!check){
        wrongVal = myScanner.nextLine(); // holds the incorrect input
        System.out.println("Invalid course number."); // prints to inform the user their input was wrong
        continue; // restarts the while loop
      }
      courseNum = myScanner.nextInt();
      enterVal = myScanner.nextLine();
    }

    // while statement to check the department name input
    while (deptName.equals(" ")){
      System.out.println("Input the department name: ");
      check = myScanner.hasNext();
      if (!check){
        wrongVal = myScanner.nextLine();
        System.out.println("Invalid department name.");
        continue;
      }
      deptName = myScanner.next();
      enterVal = myScanner.nextLine();
    }

    // while statement to check the number of course meetings input
    while (meetingNum <= 0){
      System.out.println("Input the number of times the course meets per week: ");
      check = myScanner.hasNextInt();
      if (!check){
        wrongVal = myScanner.nextLine();
        System.out.println("Invalid number of meetings.");
        continue;
      }
      meetingNum = myScanner.nextInt();
      enterVal = myScanner.nextLine();
    }

    // while statement to check the hour of the start time input
    while (timeStartHour <= 0){
      System.out.println("Input the hour of the time the class starts (ex: if starts at 10:30, input 10): ");
      check = myScanner.hasNextInt();
      if (!check){
        wrongVal = myScanner.nextLine();
        System.out.println("Invalid start time.");
        continue;
      }
      timeStartHour = myScanner.nextInt();
      enterVal = myScanner.nextLine();
    }

    // while statement to check the minutes of the start time input
    while (timeStartMin <= 0){
      System.out.println("Input the minutes of the time the class starts (ex: if starts at 10:30, input 30): ");
      check = myScanner.hasNextInt();
      if (!check){
        wrongVal = myScanner.nextLine();
        System.out.println("Invalid start time.");
        continue;
      }
      timeStartMin = myScanner.nextInt();
      enterVal = myScanner.nextLine();
    }

    // while statement to check the instructor's name input
    while (instructorName.equals(" ")){
      System.out.println("Input the instructor's name: ");
      check = myScanner.hasNext();
      if (!check){
        wrongVal = myScanner.nextLine();
        System.out.println("Invalid instructor name.");
        continue;
      }
      instructorName = myScanner.next();
      enterVal = myScanner.nextLine();
    }

    // while statement to check the number of students input
    while (studentNum <= 0){
      System.out.println("Input the number of students in the course: ");
      check = myScanner.hasNextInt();
      if (!check){
        wrongVal = myScanner.nextLine();
        System.out.println("Invalid number of students.");
        continue;
      }
      studentNum = myScanner.nextInt();
      enterVal = myScanner.nextLine();
    }
    
    // prints each of the valid values the user input
    System.out.println("The course number is " + courseNum + ".");
    System.out.println("The department name is " + deptName + ".");
    System.out.println("The number of times the course meets per week is " + meetingNum + ".");
    System.out.println("The time the class starts is " + timeStartHour + ":" + timeStartMin + ".");
    System.out.println("The instructor's name is " + instructorName + ".");
    System.out.println("The number of students in the course is " + studentNum + ".");
    
  } // end of main
} // end of class