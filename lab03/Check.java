// Chiara Hubrich 9/14/18
// CSE 02 Check Lab
// Uses the scanner class to get inputs from the user, including cost of check, tip percentage, and number of people at dinner
// and uses that data to calculate and print the cost per person for dinner

// import scanner class
import java.util.Scanner;
// define a class
public class Check{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // prints a prompt for the user to type cost
    double checkCost = myScanner.nextDouble(); // accepts the user's input and assigns to a double variable
    
    // prints a prompt for the user to type the desired tip percentage as a whole number
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble(); // accepts the user's input and assigns to a double variable
    tipPercent /= 100; // converts the tipPercent into a decimal percentage
    
    System.out.print("Enter the number of people who went out to dinner: "); // prints a prompt for the user to type the number of people
    int numPeople = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
    
    double totalCost; // the total cost of the check
    double costPerPerson; // the total cost of the check per person
    int dollars, dimes, pennies; // the whole dollar amount and the storage of digits for the tenths and hundreths places of the cost
    
    totalCost = checkCost * (1 + tipPercent); // calculates the total cost with tip
    costPerPerson = totalCost / numPeople; // finds cost each person pays
    dollars = (int) costPerPerson; // gets the whole dollar amount without the decimal
    dimes = (int) (costPerPerson * 10) % 10; // gets the dime amount (tenths decimal place)
    pennies = (int) (costPerPerson * 100) % 10; // gets the penny amount (hundreths decimal place)
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); // prints the cost per person in the form xx.xx
    
  } // end of main method
} // end of class