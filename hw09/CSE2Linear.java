// Chiara Hubrich 11/16/18
// CSE 02 CSE2Linear HW 9
// Uses multiple methods to do linear and binary searches, and also to shuffle an array of integer values input by the user

// import scanner class
import java.util.Scanner;
// import random class
import java.util.Random;
// define a class
public class CSE2Linear{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in); // construct instance of scanner
    int[] finalGrade = new int[15];
    
    String wrongVal = ""; // holds any values the user inputs that are not of the right type
    String enterVal = ""; // holds the value of the enter input when a user types in scanner
    boolean check = false; // check for if the value is an int
    
    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
    for(int i = 0; i < finalGrade.length; i++){
      check = scan.hasNextInt(); // checks that an integer is input
      while(!check){
        wrongVal = scan.next(); // if it is not an int, wrongVal holds that bad value
        System.out.println("Invalid, please input an int.");
        check = scan.hasNextInt(); // checks to see if the new input is an int
      }
      finalGrade[i] = scan.nextInt();
      while(finalGrade[i] < 0 || finalGrade[i] > 100){ // checks if the value is between 0 and 100
        System.out.println("Invalid, please input a value between 0 and 100.");
        finalGrade[i] = scan.nextInt();
      }
      if(i > 0){
        while(finalGrade[i] < finalGrade[i - 1]){ // checks that all values are greater than the one entered before it
          System.out.println("Invalid, please input an int greater than or equal to the previous int.");
          finalGrade[i] = scan.nextInt();
        }
      }
    }
    System.out.println();
    
    for(int i = 0; i < finalGrade.length; i++){
      System.out.print(finalGrade[i] + " "); // prints each element of the array
    }
    System.out.println();
    
    System.out.print("Enter a grade to search for: ");
    int searchGrade = scan.nextInt(); // user inputs the grade they want to search for
    System.out.println();
    
    int[] searchIndex = binarySearch(finalGrade, searchGrade); // binarySearch method
    
    if(searchIndex[0] == -1){ // returned if the grade value is not in the array
      System.out.println(searchGrade + " was not found in the list with " + searchIndex[1] + " iterations");
    }
    else{
      if(searchIndex[1] == 1){ // checks for if iterations should be plural or not
        System.out.println(searchGrade + " was found in the list with " + searchIndex[1] + " iteration");
      }
      else{
        System.out.println(searchGrade + " was found in the list with " + searchIndex[1] + " iterations");
      }
    }
    
    int[] scrambledGrade = scramble(finalGrade); // scramble method
    System.out.println("Scrambled: ");
    for(int i = 0; i < scrambledGrade.length; i++){
      System.out.print(scrambledGrade[i] + " "); // prints each element of the scrambled array
    }
    System.out.println();
    
    System.out.print("Enter a grade to search for: ");
    int searchGrade2 = scan.nextInt(); // user inputs another grade to search for
    System.out.println();
    
    int[] searchIndex2 = linearSearch(scrambledGrade, searchGrade2); // linearSearch method
    
    if(searchIndex2[0] == -1){ // returned if the grade value is not in the array
      System.out.println(searchGrade2 + " was not found in the list with " + searchIndex2[1] + " iterations");
    }
    else{
      if(searchIndex2[1] == 1){ // check for if iterations should be plural or not
        System.out.println(searchGrade2 + " was found in the list with " + searchIndex2[1] + " iteration");
      }
      else{
        System.out.println(searchGrade2 + " was found in the list with " + searchIndex2[1] + " iterations");
      }
    }
    
  } // end of main
  
  // performs a binary search of an array for a specific key value
  public static int[] binarySearch(int[] array, int key){
    int low = 0;
    int high = array.length - 1;
    int mid = 0;
    int counter = 0;
    int[] returns = new int[2];
    
    // used code similar to one we did in class
    while(high >= low){
      ++counter; // counts iterations
      mid = (low + high) / 2;
      if(key < array[mid]){
        high = mid - 1; // search through values below mid since the key < the value at mid
      }
      else if(key == array[mid]){
        returns[0] = mid; // holds a value different from -1 (which is mid in this case) to show the grade was found
        returns[1] = counter; // holds the number of iterations
        return returns;
      }
      else{
        low = mid + 1; // search through values above mid since key > the value at mid
      }
    }
    returns[0] = -1; // returned if the value is not found
    returns[1] = counter;
    return returns;
  }
  
  // performs a linear search of an array for a specific key value
  public static int[] linearSearch(int[] array, int key){
    int counter = 0;
    int[] returns = new int[2];
    for(int i = 0; i < array.length; i++){ // iterates through each value starting at the lowest, rather than using middle values
      ++counter; // counts iterations
      if(key == array[i]){
        returns[0] = i; // returns value because the key was found
        returns[1] = counter;
        return returns;
      }
    }
    returns[0] = -1;
    returns[1] = counter;
    return returns;
  }
  
  // scrambles the elements of an array of integers
  public static int[] scramble(int[] array){
    Random randGen = new Random(); // construct instance of random
    int tempVal = 0;
    int randVal = 0;
    for(int i = 0; i < 30; i++){ // scrambles values 30 times
      randVal = randGen.nextInt(13) + 1; // chooses a random index to swap values with index 0
      tempVal = array[0];
      array[0] = array[randVal];
      array[randVal] = tempVal;
    }
    return array;
  }
  
} // end of class