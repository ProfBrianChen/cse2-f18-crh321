// Chiara Hubrich 11/19/18
// CSE 02 RemoveElements HW 9
// Randomly generates a 10 element array with random values between 0 and 9, then uses methods and asks the user for an index to delete
// (checking to see if the index is valid) and removing an element value from all indexes containing that value for the array

// import scanner class
import java.util.Scanner;
// define a class
public class RemoveElements{
  // main method required for every java program
  public static void main(String[] arg){
    
	Scanner scan = new Scanner(System.in); // construct instance of scanner
	
    int num[] = new int[10];
    int newArray1[];
    int newArray2[];
    int index ,target;
	String answer = "";
	
	do{
  	System.out.print("Random input 10 ints [0-9] ");
  	num = randomInput(); // randomInput method
  	String out = "The original array is:";
  	out += listArray(num); // return a string of the form "{2, 3, -9}"
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num, index); // delete method
  	String out1 = "The output array is ";
  	out1 += listArray(newArray1); // return a string of the form "{2, 3, -9}"
  	System.out.println(out1);
 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target); // remove method
  	String out2 = "The output array is ";
  	out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer = scan.next();
	}while(answer.equals("Y") || answer.equals("y")); // if the user inputs "Y" or "y" then the loop will run again
  } // end of main

  // creates the printable array as a list of values between curly brackets
  public static String listArray(int num[]){
	String out = "{";
	for(int j = 0;j < num.length; j++){ // creates a list of the array values as a String
  	  if(j > 0){
    	out += ", ";
  	  }
  	out += num[j];
	}
	out += "} ";
	return out;
  }
  
  // generates a 10 element array of random ints 0-9
  public static int[] randomInput(){
    int[] randArray = new int[10];
    for(int i = 0; i < randArray.length; i++){
      randArray[i] = (int)(Math.random() * 10); // randomly generates values 0-9
    }
    return randArray;
  }
  
  // deletes an index and creating a new array without that value
  public static int[] delete(int[] list, int pos){
    int[] deleteList = new int[9];
    if(pos < 0 || pos > 9){ // checks that the index is betweeen 0 and 9
      System.out.println("The index is not valid.");
      return list;
    }
    for(int i = 0; i < pos; i++){
      deleteList[i] = list[i]; // for values before the deleted index, they are the same in the next array as in the old
    }
    for(int i = deleteList.length - 1; i >= pos; i--){
      deleteList[i] = list[i + 1]; // for values after the deleted index, the i + 1 index of the original is the ith index of the new array
    }
    System.out.println("Index " + pos + " is removed.");
    return deleteList;
  }
  
  // removes all occurences of an element value in the array, creating a new array without all of those instances of that value
  public static int[] remove(int[] list, int target){
    int counter = 0;
    for(int i = 0; i < list.length; i++){
      if(list[i] == target){
        counter++; // counts the number of times the element occurs
      }
    }
    if(counter == 0){ // if it occurs no times, then the element is not in the array
      System.out.println("Element " + target + " was not found.");
      return list;
    }
    
    int[] removeList = new int[list.length - counter]; // creates an array to hold the values that don't include the removed element
    int[] badVals = new int[counter];
    int j = 0, k = 0, counter2 = 0;
    for(int i = 0; i < list.length; i++){
      if(list[i] == target){ // when the element is the target, it will go into the badVals array to hold the removed element values
        badVals[j] = list[i];
        j++; // increases the index of badVals when this if statement is passed through
      }
      else{
        removeList[k] = list[i];
        k++; // increases the index of the new list of values without the reomved element when this else statement is passed through
      }
    }
    System.out.println("Element " + target + " has been found.");
    return removeList; // returns the array with the values not equal to the target
  }
  
} // end of class