//Chiara Hubrich 9/4/2018
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    //Prints Welcome text and design to terminal window
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    //Prints the arrow design and my lehigh network ID to the terminal window
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-C--R--H--3--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    //Prints my autobiographic statement to the terminal window
    System.out.println("I'm Chiara Hubrich from Quakertown, PA. I'm a 19 year old math major at Lehigh University.");
  }
}