// Chiara Hubrich 10/30/18
// CSE 02 hw07 HW 7
// Use scanner class for user to input a sample text, then print their text and a menu with 6 different options for the user
// to select from (quit, number of words, find word, etc.) using multiple methods

// import scanner class
import java.util.Scanner;
// define a class
public class hw07{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    int holder = 0; // holder place value to keep loop going until user selects quit
    String st = sampleText(); // calls method to hold the value of the text input by the user
    char pm = printMenu(); // calls methos to hold value of the menu
    
    // while loop to allow user to continue selecting from menu until they quit
    while (holder == 0){
      if (pm == 'q'){ // path taken if user wants to quit
        break;
      }
      else if (pm == 'c'){ // path taken to count the non-whitespace characters
        int nonWSChar = getNumOfNonWSCharacters(st); // uses method to get number of non-whitespace characters
        System.out.println("Number of non-whitespace characters: " + nonWSChar);
        pm = printMenu(); // prints menu again
        continue;
      }
      else if (pm == 'w'){ // path taken to count number of words
        int numWords = getNumOfWords(st);
        System.out.println("Number of words: " + numWords);
        pm = printMenu();
        continue;
      }
      else if (pm == 'f'){ // path taken to find word
        System.out.println("Enter a word or phrase to be found:");
        String findText = myScanner.nextLine(); // asks for word they want to find
        int numText = findText(findText, st);
        System.out.println("'" + findText + "' instances: " + numText);
        pm = printMenu();
        continue;
      }
      else if (pm == 'r'){ // path taken to replace '!' with '.'
        String fixText = replaceExclamation(st);
        System.out.println("Edited text: " + fixText);
        pm = printMenu();
        continue;
      }
      else if (pm == 's'){ // path taken to shorten length of 2 or more spaces
        String fixSpace = shortenSpace(st);
        System.out.println("Edited text: " + fixSpace);
        pm = printMenu();
        continue;
      }
      else{ // path taken if an invalid input is typed by the user and asks for a valid one
        System.out.println("");
        System.out.println("Please type a valid input.");
        pm = printMenu();
        continue;
      }
    }
    
  } // end of main

  // sampleText method
  public static String sampleText(){
    // asks for user to input sample text, then prints that text and returns it
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter a sample text:");
    String sampleText = myScanner.nextLine();
    System.out.println(" ");
    System.out.println("You entered: " + sampleText);
    return sampleText;
  }

  // printMenu method
  public static char printMenu(){
    // prints the menu and asks the user to input the character they want
    Scanner myScanner = new Scanner(System.in);
    System.out.println("");
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("");
    System.out.println("Choose an option:");
    
    String userInput = myScanner.next();
    char menuVal = userInput.charAt(0);
    return menuVal;
  }
  
  // getNumOfNonWSCharacters method
  public static int getNumOfNonWSCharacters(String sampleText){
    int sampleTextLength = sampleText.length();
    int charCounter = 0;
    
    // loops for the length of the sample text
    for(int counter = 0; counter <= sampleTextLength - 1; counter++){
      char charPosition = sampleText.charAt(counter); // loops through the character at each position in the word
      if (charPosition != ' '){ // if the character is a not a space, then it is non-whitespace, so it adds to the counter
        charCounter++;
      }
    }
    return charCounter; // return the counter for non-whitespace characters
  }

  // getNumOfWords method
  public static int getNumOfWords(String sampleText){
    // if there are no characters, return 0
    if (sampleText == null || sampleText.isEmpty()){
      return 0; 
    }
    
    // split the String on the whitespace, which divides words
    String[] words = sampleText.split("\\s+");
    return words.length;
  }
  
  // findText method
  public static int findText(String textToFind, String sampleText){
    int lastIndex = 0;
    int counter = 0;
    
    // loops through the sample text to find the index values of the characters in the text to find
    while(lastIndex != -1){
      lastIndex = sampleText.indexOf(textToFind, lastIndex);
      
      if(lastIndex != -1){
        counter++;
        lastIndex += textToFind.length();
      }
    }
    return counter; // returns the number of times the index values are found
  }
  
  // replaceExclamation method
  public static String replaceExclamation(String sampleText){
    // replaces '!' with '.'
    String fixText = sampleText.replace('!', '.');
    return fixText; // returns the sample text with the changes
  }
  
  // shortenSpace method
  public static String shortenSpace(String sampleText){
    // replaces sets of spaces of more than 1 space with a single space
    String fixSpace = sampleText.trim().replaceAll(" +", " ");
    return fixSpace; // returns the sample text with the changes
  }


} // end of class