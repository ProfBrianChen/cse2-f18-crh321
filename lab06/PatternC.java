// Chiara Hubrich 10/12/18
// CSE 02 PatternC Lab 6
// Uses the scanner class to get an integer 1-10 from the user (and checks to see that the value is an integer 1-10), and then creates
// PatternC based on the value input by the user using nested for-loops

// import scanner class
import java.util.Scanner;
// define a class
public class PatternC{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    
    int tempVal = 0; // temporary holder for value of inputRows
    int inputRows = 0; // variable for the user input value of the number of rows in the pattern
    String wrongVal = ""; // holds any values the user inputs that are not of the right type
    String enterVal = ""; // holds the value of the enter input when a user types in scanner
    boolean check = false; // the boolean value to check for the correct type in hasNextInt
    
    // while statement to check that the row number is in the range 1-10 and an integer
    System.out.println("Input an integer 1-10 for the number of rows in the pattern: ");
    while (inputRows <= 0){
      check = myScanner.hasNextInt();
      if (!check){ // checks if int value
        wrongVal = myScanner.nextLine(); // holds the incorrect input
        System.out.println("Invalid input. Please input an integer 1-10: "); // prints to inform the user their input was wrong
        continue; // restarts the while loop
      }
      else if (check){
        tempVal = myScanner.nextInt();
        if (tempVal > 10 || tempVal < 1){ // checks if in range 1-10
          wrongVal = myScanner.nextLine();
          System.out.println("Invalid input. Please input an integer 1-10: ");
          continue;
        }
        inputRows = tempVal;
        enterVal = myScanner.nextLine();
      }
    }
    
    for (int numRows = 1; numRows <= inputRows; numRows++){ // iterates through each row at a time, incrementing up until the user input
        for (int spaceRow = inputRows - numRows - 1; spaceRow >= 0; spaceRow--){
            System.out.print(" ");
        }
        for (int perRow = 1; perRow <= numRows; perRow++){ // outputs 1 through numRows, the number of the row that the iteration of the first for-loop is on
            System.out.print(numRows - perRow + 1);
        }
        System.out.println(); // outputs the new line between each row of the pattern
    }
    
  } // end of main
} // end of class