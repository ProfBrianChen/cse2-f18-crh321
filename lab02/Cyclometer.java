// Chiara Hubrich 9/7/18
// CSE 02 Cyclometer Lab
// Prints the number of minutes and counts for each trip, the distance in miles for each trip in miles,
// and the distance of the two trips combined in miles

// define a class
public class Cyclometer{
  // main method required for every Java program
  public static void main(String[] args){
    
    int secsTrip1 = 480; // number of seconds for trip 1
    int secsTrip2 = 3220; // number of seconds for trip 2
  	int countsTrip1 = 1561; // number of counts for trip 1
  	int countsTrip2 = 9037; // number of counts for trip 2
    
    double wheelDiameter = 27.0,  // the diameter of the bicycle wheel
  	PI = 3.14159, // constant pi
  	feetPerMile = 5280,  // number of feet in a mile 
  	inchesPerFoot = 12,   // number of inches in a foot
  	secondsPerMinute = 60;  // number of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  // total distance variables for trips 1, 2, and the total of both combined
    
    // prints the minutes and counts for trips 1 and 2
    System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
	  System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // calculates for trip 1 the distance in inches with the diameter, pi, and counts
	  distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // calculates distance in inches for trip 1, the converts to miles
	  totalDistance = distanceTrip1 + distanceTrip2; // sums the distances of trips 1 and 2 to calculate total distance
    
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); // prints distance of trip 1 in miles
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles"); // prints distance of trip 2 in miles
	  System.out.println("The total distance was " + totalDistance + " miles"); // prints total distance in miles
    
  } // end of main method
} // end of class