// Chiara Hubrich 9/17/18
// CSE 02 Convert HW 3
// Uses the scanner class to ask the user for inputs of inches of rain and acres affected, then converts the inches and acres
// into cubic miles of rain

// import scanner class
import java.util.Scanner;
// define a class
public class Convert{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    
    System.out.print("Enter the affected area in acres: "); // prints a prompt for the user to type the affected area in acres
    double areaAcres = myScanner.nextDouble(); // accepts the user's input and assigns to a double variable
    
    System.out.print("Enter the rainfall in the affected area in inches: "); // prints a prompt for the user to type the rainfall in inches
    double rainfallInches = myScanner.nextDouble(); // accepts the user's input and assigns to a double variable
    
    double gallonConvert = 27154.3; // conversion factor for going from acre-inches to gallons
    double cubicMileConvert = 9.0817E-13; // conversion factor for going from gallons to cubic miles
    
    double acreInches = areaAcres * rainfallInches; // calculates acre-inches
    double rainfallGallons = acreInches * gallonConvert; // converts acre-inches to gallons
    double cubicMiles = rainfallGallons * cubicMileConvert; // converts gallons to cubic miles
    
    System.out.println(cubicMiles); // prints the quantity of rain in cubic miles
    
  } // end of main
} // end of class