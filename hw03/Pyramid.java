// Chiara Hubrich 9/14/18
// CSE 02 Pyramid HW 3
// Uses the scanner class to ask for inputs from the user on the lengths and height of a pyramid and uses the data
// to calculate the volume of the pyramid

// import scanner class
import java.util.Scanner;
// define a class
public class Pyramid{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    
    System.out.print("The length of a square side of the pyramid is (input length): "); // prints a prompt for the user to type the square side length
    double squareSide = myScanner.nextDouble(); // accepts the user's input and assigns to a double variable
    
    System.out.print("The height of the pyramid is (input height): "); // prints a prompt for the user to type the height of the pyramid
    double height = myScanner.nextDouble(); // accepts the user's input and assigns to a double variable
    
    double volumePyramid = (squareSide * squareSide * height) / 3; // calculates volume of a pyramid
    System.out.println("The volume inside the pyramid is: " + volumePyramid + "."); // prints the pyramid volume

  } // end of main method
} // end of class