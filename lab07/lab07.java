// Chiara Hubrich 10/21/18
// CSE 02 lab07 Lab 7
// Creates sentences by using multiple methods with adjectives, subjects, objects, and verbs to create multiple strings that are combined
// to form sentences that become a paragraph

// import scanner class
import java.util.Scanner;
// import random class
import java.util.Random;
// define a class
public class lab07{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    Random randGen = new Random(); // construct instance of scanner
    
    boolean nextSentence = true;
    String actionParagraph = "";
    String s1 = "";
    String thesis = "";
    
    // while loop that forms the thesis sentence and then asks if the user wants a different sentence, looping until they like the sentence
    while(nextSentence){
      int adj1 = 0, adj2 = 0, adj3 = 0, sub1 = 0, ve1 = 0, obj1 = 0;
      String a1 = adjective(adj1);
      String a2 = adjective(adj2);
      String a3 = adjective(adj3);
      s1 = subject(sub1);
      String v1 = verb(ve1);
      String o1 = object(obj1);
      
      // stops 2 adjectives at the beginning from being the same
      while(a1 == a2){
        a2 = adjective(adj2);
      }
      
      thesis = "The " + a1 + " " + a2 + " " + s1 + " " + v1 + " the " + a3 + " " + o1 + "."; // creates thesis sentence
      System.out.println(thesis);
      System.out.println("If you would like another sentence, type 'true'. If not, type 'false'.");
      nextSentence = myScanner.nextBoolean();
    }
    
    // randomly generates how many action sentences with be used
    int numAction = randGen.nextInt(10);
    for(int counter = 0; counter <= numAction; counter++){
      actionParagraph += actionSentence(s1); // adds each action sentence onto the last
    }
    
    String conclusion = conclusionSentence(s1); // calls method to create conclusion sentence
    
    System.out.println(fullPara(thesis, actionParagraph, conclusion)); // creates paragraph
  } // end of main

  // actionSentence method
  public static String actionSentence(String origSub){
    Random randGen = new Random();
    int subjectOrIt = randGen.nextInt(2); // randomly generates 0 or 1 to decide whether the subject or it will be used
    
    // calls methods that will be used to pick the words in the sentence
    int adj1 = 0, ve1 = 0, obj1 = 0, obj2 = 0, obj3 = 0;
    String a1 = adjective(adj1);
    String v1 = verb(ve1);
    String o1 = object(obj1);
    String o2 = object(obj2);
    String o3 = object(obj3);
    
    // prevents overlapping words
    while(o1 == o2 || o2 == o3 || o1 == o3){
      o2 = object(obj2);
      o3 = object(obj3);
    }
    
    // if statement for the 2 types of action sentences, return's whicheven one is chosen
    if(subjectOrIt == 0){
      String subAction = "This " + origSub + " was particularly " + a1 + " to " + v1 + " " + o1 + ". ";
      return subAction;
    }
    else{
      String itAction = "It used " + o1 + " when it " + v1 + " " + o2 + " at the " + a1 + " " + o3 + ". ";
      return itAction;
    }
  }
  
  // conclusionSentence method
  public static String conclusionSentence(String origSub){
    int ve1 = 0, obj1 = 0;
    String v1 = verb(ve1);
    String o1 = object(obj1);
    
    // returns conclusion sentence
    String conclusion = "That " + origSub + " " + v1 + " her " + o1 + "!";
    return conclusion;
  }
  
  // fullPara method
  public static String fullPara(String thesis, String action, String conclusion){
    String paragraph = thesis + " " + action + conclusion; // combines all of the sentence sets into one paragraph and returns it
    return paragraph;
  }
  
  // adjective method
  public static String adjective(int num){
    Random randGen = new Random();
    num = randGen.nextInt(10); // generates a random number from 0-9
    String adj = " ";
    switch (num){ // assigns the random number to an adjective string
      case 0: adj = "happy";
        break;
      case 1: adj = "colorful";
        break;
      case 2: adj = "angry";
        break;
      case 3: adj = "quick";
        break;
      case 4: adj = "interesting";
        break;
      case 5: adj = "joyful";
        break;
      case 6: adj = "shy";
        break;
      case 7: adj = "proud";
        break;
      case 8: adj = "rude";
        break;
      case 9: adj = "excited";
        break;
      default:
        break;
    }
    return adj; // returns the adjective
  }
  
  // subject method
  public static String subject(int num){
    Random randGen = new Random();
    num = randGen.nextInt(10); // generates a random number from 0-9
    String sub = " ";
    switch (num){ // assigns the random number to an subject string
      case 0: sub = "cat";
        break;
      case 1: sub = "woman";
        break;
      case 2: sub = "child";
        break;
      case 3: sub = "zebra";
        break;
      case 4: sub = "clam";
        break;
      case 5: sub = "flower";
        break;
      case 6: sub = "lion";
        break;
      case 7: sub = "sea horse";
        break;
      case 8: sub = "kangaroo";
        break;
      case 9: sub = "tree";
        break;
      default:
        break;
    }
    return sub; // returns the subject
  }
  
  // verb method
  public static String verb(int num){
    Random randGen = new Random();
    num = randGen.nextInt(10); // generates a random number from 0-9
    String ve = " ";
    switch (num){ // assigns the random number to an verb string
      case 0: ve = "kicked";
        break;
      case 1: ve = "jumped";
        break;
      case 2: ve = "ran";
        break;
      case 3: ve = "read";
        break;
      case 4: ve = "examined";
        break;
      case 5: ve = "called";
        break;
      case 6: ve = "questioned";
        break;
      case 7: ve = "led";
        break;
      case 8: ve = "loved";
        break;
      case 9: ve = "appreciated";
        break;
      default:
        break;
    }
    return ve; // returns the verb
  }
  
  // object method
  public static String object(int num){
    Random randGen = new Random();
    num = randGen.nextInt(10); // generates a random number from 0-9
    String obj = " ";
    switch (num){ // assigns the random number to an object string
      case 0: obj = "sea shells";
        break;
      case 1: obj = "rockets";
        break;
      case 2: obj = "penguins";
        break;
      case 3: obj = "books";
        break;
      case 4: obj = "puzzles";
        break;
      case 5: obj = "computers";
        break;
      case 6: obj = "dogs";
        break;
      case 7: obj = "shoes";
        break;
      case 8: obj = "cars";
        break;
      case 9: obj = "rivers";
        break;
      default:
        break;
    }
    return obj; // returns the object
  }
  
} // end of class