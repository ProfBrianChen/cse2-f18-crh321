// Chiara Hubrich 11/16/18
// CSE 02 lab09 Lab 9
// Uses multiple different methods to show how to pass arrays as method arguments, including methods that copy, reverse, and print arrays

// define a class
public class lab09{
  // main method required for every java program
  public static void main(String[] args){
    // declaring the initial array and making 2 copies of it
    int[] array0 = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    
    inverter(array0); // passes array0 through the inverter method
    inverter2(array1); // passes array1 through the inverter2 method
    int[] array3 = inverter2(array2); // passes array2 through the inverter2 method and assigns it to array3
    
    // prints the 3 arrays we need using the print method
    print(array0);
    print(array1);
    print(array3);
  } // end of main
  
  // copy method creates a copy of an integer array
  public static int[] copy(int[] arrayIn){
    int[] arrayOut = new int[arrayIn.length]; // creates the variable for the copy array
    for(int i = 0; i < arrayOut.length; i++){
      arrayOut[i] = arrayIn[i]; // assigns each element of the original array to the element of that same index in the copy
    }
    return arrayOut; // returns the copy
  }
  
  // inverter method reverses the elements of an integer array
  public static void inverter(int[] arrayIn){
    int tempVal = 0;
    for(int i = 0; i < (arrayIn.length / 2); i++){ // only needs to run for the first half of the array to finish
      tempVal = arrayIn[i]; // holds the value of the original array
      arrayIn[i] = arrayIn[arrayIn.length - 1 - i]; // assigns the original to the value on the opposite half of the array (1st element = last element)
      arrayIn[arrayIn.length - 1 - i] = tempVal; // assigns the held value
    }
  }
  
  // inverter2 method creates a copy of an array and then reverses the elements of the copy
  public static int[] inverter2(int[] arrayIn){
    int[] arrayOut = copy(arrayIn); // creates a copy of the input array
    int tempVal = 0;
    for(int i = 0; i < (arrayOut.length / 2); i++){ // reverses the copy of the array
      tempVal = arrayOut[i];
      arrayOut[i] = arrayOut[arrayOut.length - 1 - i];
      arrayOut[arrayOut.length - 1 - i] = tempVal;
    }
    return arrayOut; // returns the reversed copy
  }
  
  // print method prints each element of an array
  public static void print(int[] arrayIn){
    for(int i = 0; i < arrayIn.length; i++){
      System.out.print(arrayIn[i] + " "); // runs through each element of the array an prints with a space after each
    }
    System.out.println();
  }
  
} // end of class