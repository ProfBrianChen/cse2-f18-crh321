// Chiara Hubrich 9/21/18
// CSE 02 CrapsIf HW 4
// Uses the scanner and random classes to allow a user to randomly get or manually input values of 2 dice values, then assigns those values to
// the slang terms of rolls from Craps using if and if-else statements

// import scanner class
import java.util.Scanner;
//import random class
import java.util.Random;
// define a class
public class CrapsIf{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    Random randGen = new Random(); // new random number generator
    
    // prints prompt for user to type whether they want to have to 2 dice randomly cast or if they want to choose the values
    System.out.println("Please indicate 'true' if you would like the dice randomly cast. If you would like to select the two values, indicate 'false'.");
    boolean castDice = myScanner.nextBoolean(); // accepts the user's input and assigns to a boolean variable
    
    int roll1, roll2; // the values of the two dice rolls
    String slangTerm; // the name of the slang term
    
    if (castDice){ // if the user inputs 'true', then this branch is taken
      // randomly generates 2 integers 1-6 for the dice
      roll1 = randGen.nextInt(6) + 1;
      roll2 = randGen.nextInt(6) + 1;
      System.out.println("The two outcomes from the dice are " + roll1 + " and " + roll2 + "."); // prints the values of the dice rolls
    }
    else{ // if the user does not input true, this branch is taken
      System.out.print("Enter the value of one die roll: "); // prints prompt for user to input one of the die values
      roll1 = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
      
      System.out.print("Enter the value of the other die roll: "); // prints prompt for user to input the other of the die values
      roll2 = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
    }
    
    // uses if-else statement to prevent using values the user may input outside of the range 1-6
    if (roll1 >= 1 && roll1 <= 6 && roll2 >= 1 && roll2 <= 6){
      // uses if and else-if statements to assign the sum of the two rolls to a slang term, then prints a statement stating the term
      if (roll1 + roll2 == 2){
        slangTerm = "Snake Eyes";
        System.out.println("This roll is called a " + slangTerm + ".");
      }
      else if (roll1 + roll2 == 3){
        slangTerm = "Ace Deuce";
        System.out.println("This roll is called a " + slangTerm + ".");
      }
      else if (roll1 + roll2 == 4){
        // used to differentiate when the two rolls sum to a number, but the roll values are not equivalent, giving it different slang terms
        if (roll1 == roll2){
          slangTerm = "Hard Four";
          System.out.println("This roll is called a " + slangTerm + ".");
        }
        else{
          slangTerm = "Easy Four";
          System.out.println("This roll is called a " + slangTerm + ".");
        }
      }
      else if (roll1 + roll2 == 5){
        slangTerm = "Fever Five";
        System.out.println("This roll is called a " + slangTerm + ".");
      }
      else if (roll1 + roll2 == 6){
        // used to differentiate when the two rolls sum to a number, but the roll values are not equivalent, giving it a different slang term
        if (roll1 == roll2){
          slangTerm = "Hard Six";
          System.out.println("This roll is called a " + slangTerm + ".");
        }
        else{
          slangTerm = "Easy Six";
          System.out.println("This roll is called a " + slangTerm + ".");
        }
      }
      else if (roll1 + roll2 == 7){
        slangTerm = "Seven Out";
        System.out.println("This roll is called a " + slangTerm + ".");
      }
      else if (roll1 + roll2 == 8){
        // used to differentiate when the two rolls sum to a number, but the roll values are not equivalent, giving it a different slang term
        if (roll1 == roll2){
          slangTerm = "Hard Eight";
          System.out.println("This roll is called a " + slangTerm + ".");
        }
        else{
          slangTerm = "Easy Eight";
          System.out.println("This roll is called a " + slangTerm + ".");
        }
      }
      else if (roll1 + roll2 == 9){
        slangTerm = "Nine";
        System.out.println("This roll is called a " + slangTerm + ".");
      }
      else if (roll1 + roll2 == 10){
        // used to differentiate when the two rolls sum to a number, but the roll values are not equivalent, giving it a different slang term
        if (roll1 == roll2){
          slangTerm = "Hard Ten";
          System.out.println("This roll is called a " + slangTerm + ".");
        }
        else{
          slangTerm = "Easy Ten";
          System.out.println("This roll is called a " + slangTerm + ".");
        }
      }
      else if (roll1 + roll2 == 11){
        slangTerm = "Yo-leven";
        System.out.println("This roll is called a " + slangTerm + ".");
      }
      else if (roll1 + roll2 == 12){
        slangTerm = "Boxcars";
        System.out.println("This roll is called a " + slangTerm + ".");
      }
    }
    else{ // prints 'Invalid entry' when the value is outside of the range of die values
      System.out.println("Invalid entry.");
    }
    
  } // end of main
} // end of class