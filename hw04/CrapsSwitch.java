// Chiara Hubrich 9/24/18
// CSE 02 CrapsSwitch HW 4
// Uses the scanner and random classes to allow a user to randomly get or manually input values of 2 dice values, then assigns those values to
// the slang terms of rolls from Craps using switch statements

// import scanner class
import java.util.Scanner;
//import random class
import java.util.Random;
// define a class
public class CrapsSwitch{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    Random randGen = new Random(); // new random number generator
    
    // prints prompt for user to type whether they want to have to 2 dice randomly cast or if they want to choose the values
    System.out.println("Please indicate '1' if you would like the dice randomly cast. If you would like to select the two values, indicate '2'.");
    int castDice = myScanner.nextInt(); // accepts the user's input and assigns to a boolean variable
    
    int roll1 = 0, roll2 = 0; // the values of the two dice rolls
    String slangTerm; // the name of the slang term
    
    // switch statement based on user decision of random dice roll or their own input of dice values
    switch (castDice){
      case 1: // if user inputs '1', then this branch is taken
        // randomly generates 2 integers 1-6 for the dice
        roll1 = randGen.nextInt(6) + 1;
        roll2 = randGen.nextInt(6) + 1;
        System.out.println("The two outcomes from the dice are " + roll1 + " and " + roll2 + "."); // prints the values of the dice rolls
        break;
      case 2: // if user inputs '2', then this branch is taken
        System.out.print("Enter the value of one die roll: "); // prints prompt for user to input one of the die values
        roll1 = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
        System.out.print("Enter the value of the other die roll: "); // prints prompt for user to input the other of the die values
        roll2 = myScanner.nextInt(); // accepts the user's input and assigns to an int variable
        break;
      default: System.out.println("Invalid entry."); // if the user inputs anything other than 1 or 2, this is taken and prints that it is invalid
        break;
    }
        
    int rollSum = roll1 + roll2; // the sum of the two roll values
    
    // used to keep range of roll1 values to dice values 1-6
    switch(roll1){
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6: roll1 = roll1;
        break; // break is here because for all values 1-6, the roll value will be kept as it is
      default: rollSum = 0; // used to invalidate any roll entry outside of 1-6
        System.out.println("Invalid entry.");
        break;
    }
        
    // used to keep range of roll2 values to dice values 1-6
    switch(roll2){
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6: roll2 = roll2;
        break; // break is here because for all values 1-6, the roll value will be kept as it is
      default: rollSum = 0; // used to invalidate any roll entry outside of 1-6
        System.out.println("Invalid entry.");
        break;
    }
    
    // switch statement to assign the slang terminology to its dice values
    switch (rollSum){
      case 2: slangTerm = "Snake Eyes"; // assigns slang term to a specific sum of the two dice
        System.out.println("This roll is called a " + slangTerm + "."); // prints statement stating the slang term
        break;
      case 3: slangTerm = "Ace Deuce";
        System.out.println("This roll is called a " + slangTerm + ".");
        break;
      case 4:
        switch(roll1){ // nested switch statement to differentiate the roll values for Hard vs. Easy slang terms
          case 2: slangTerm = "Hard Four"; // when roll1 = 2, so will roll2 (since the sum is 4, which is "Hard Four" in Craps
            System.out.println("This roll is called a " + slangTerm + ".");
              break;
          default: slangTerm = "Easy Four"; // for any other sum of 4 that is not 2 and 2, the name is different, which is why it's the default
            System.out.println("This roll is called a " + slangTerm + ".");
              break;
        }
        break;
      case 5: slangTerm = "Fever Five";
        System.out.println("This roll is called a " + slangTerm + ".");
        break;
      case 6:
        switch(roll1){
          case 3: slangTerm = "Hard Six";
            System.out.println("This roll is called a " + slangTerm + ".");
              break;
          default: slangTerm = "Easy Six";
            System.out.println("This roll is called a " + slangTerm + ".");
              break;
        }
        break;
      case 7: slangTerm = "Seven Out";
        System.out.println("This roll is called a " + slangTerm + ".");
        break;
      case 8:
        switch(roll1){
          case 4: slangTerm = "Hard Eight";
            System.out.println("This roll is called a " + slangTerm + ".");
              break;
          default: slangTerm = "Easy Eight";
            System.out.println("This roll is called a " + slangTerm + ".");
              break;
        }
        break;
      case 9: slangTerm = "Nine";
        System.out.println("This roll is called a " + slangTerm + ".");
        break;
      case 10:
        switch(roll1){
          case 5: slangTerm = "Hard Ten";
            System.out.println("This roll is called a " + slangTerm + ".");
              break;
          default: slangTerm = "Easy Ten";
            System.out.println("This roll is called a " + slangTerm + ".");
              break;
        }
        break;
      case 11: slangTerm = "Yo-leven";
        System.out.println("This roll is called a " + slangTerm + ".");
        break;
      case 12: slangTerm = "Boxcars";
        System.out.println("This roll is called a " + slangTerm + ".");
        break;
      default: // for situation where roll1 + roll2 are none of the sums found in the game, the roll(s) are outside of the range and print invalid
        System.out.println("Invalid entry.");
        break;
    }
        
  } // end of main
} // end of class