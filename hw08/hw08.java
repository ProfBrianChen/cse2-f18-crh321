// Chiara Hubrich 11/12/18
// CSE 02 hw08 HW 8
// Uses multiple methods and arrays to shuffle a string of arrays that represent a deck of cards, and then make a hand of cards
// from 5 cards in the shuffled deck, using the scanner class to ask the user whether they want another hand or not

// import scanner class
import java.util.Scanner;
// define a class
public class hw08{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in); // construct instance of scanner
    
    // creates arrays with the suits and card ranks, and arrays for the cards and hand values
    String[] suitNames = {"C", "H", "S", "D"};
    String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
    String[] cards = new String[52];
    String[] hand = new String[5];
    
    System.out.println("Enter the number of cards in a hand: "); // asks user to input a value for the card hand
    int numCards = scan.nextInt();
    int again = 1;
    int index = 51;
    
    // for loop makes the cards array hold the values of all 52 card types, assigning each index to a different card and printing it out
    for (int i = 0; i < 52; i++){
      cards[i] = rankNames[i % 13] + suitNames[i / 13];
      System.out.print(cards[i] + " ");
    }
    System.out.println();
    printArray(cards); // printArray method (this was in the given method so I didn't change it, but prints the cards twice)
    shuffle(cards); // shuffle method
    printArray(cards); // printArray method
    
    // creates the hand of cards and asks the user if they want another hand in a while loop
    while(again == 1){
      hand = getHand(cards, index, numCards); // getHand method
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn");
      again = scan.nextInt();
    }
  } // end of main
  
  // prints each element of the array input with a space in between
  public static void printArray(String[] list){
    for(int i = 0; i < list.length; i++){
      System.out.print(list[i] + " ");
    }
    System.out.println();
  }
  
  // shuffles the array of card values
  public static String[] shuffle(String[] list){
    String tempVal = "";
    int randVal = 0;
    for(int i = 0; i < 100; i++){ // shuffles through 100 times
      randVal = (int)(Math.random() * 51 + 1); // chooses a random index to swap values with index 0
      tempVal = list[0];
      list[0] = list[randVal];
      list[randVal] = tempVal;
    }
    System.out.println("Shuffled");
    return list; // returns the newly shuffled deck of cards as an array
  }
  
  // takes numCards amount of cards from the end of the shuffled array
  public static String[] getHand(String[] list, int index, int numCards){
    String[] hand = new String[numCards]; // creates array to hold the values of the hand
    int j = 0;
    for(int i = index; i > (index - numCards); i--){ // goes from the value of the index down to the index minus numCards
      hand[j] = list[i]; // assigns the last numCards card values to the hand array
      ++j;
    }
    System.out.println("Hand");
    return hand; // returns the hand array
  }
  
} // end of class