// Chiara Hubrich 9/7/2018
// CSE 02 Arithmetic
// Calculates and prints the cost of purchases of pants, shirts, and belts before sales tax, with sales tax, 
// and the value of the total sales tax charged.

// define a class
public class Arithmetic{
  // main method required for every Java program
  public static void main(String[] args){
    
    int numPants = 3; // number of pairs of pants
    double pantsPrice = 34.98; // cost per pair of pants
    int numShirts = 2; // number of sweatshirts
    double shirtPrice = 24.99; // cost per shirt
    int numBelts = 1; // number of belts
    double beltPrice = 33.99; // cost per belt
    double paSalesTax = 0.06; // the tax rate
    
    double totalCostOfPants; // total cost of pants
    double totalCostOfShirts; // total cost of shirts
    double totalCostOfBelts; // total cost of belts
    double salesTaxPants; // sales tax of pants
    double salesTaxShirts; // sales tax of shirts
    double salesTaxBelts; // sales tax of belts
    double preTaxTotal; // total cost before tax
    double totalSalesTax; // total sales tax cost
    double totalCost; // total cost including tax
    
    totalCostOfPants = numPants * pantsPrice; // calculates the total cost of pants
    totalCostOfShirts = numShirts * shirtPrice; // calculates the total cost of shirts
    totalCostOfBelts = numBelts * beltPrice; // calculates the total cost of belts
    
    salesTaxPants = totalCostOfPants * paSalesTax; // calculates the sales tax on pants
    salesTaxShirts = totalCostOfShirts * paSalesTax; // calculates the sales tax on shirts
    salesTaxBelts = totalCostOfBelts * paSalesTax; // calculates the sales tax on belts
    
    preTaxTotal = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // sums each clothing cost to calculate the total cost before tax
    totalSalesTax = (salesTaxPants + salesTaxShirts + salesTaxBelts) * 100;  // calculating the total sales tax
    totalSalesTax = (int) totalSalesTax / 100.0; // converting totalSalesTax in order to have only two decimal places in the final answer
    totalCost = (preTaxTotal + totalSalesTax) * 100; // calculating the total cost
    totalCost = (int) totalCost / 100.0; // converting totalCost in order to have only two decimal places in the final answer
    
    System.out.println("The cost of the purchases before tax is $" + preTaxTotal); // prints the total cost before tax
    System.out.println("The total sales tax is $" + totalSalesTax); // prints the total sales tax
    System.out.println("The total cost of the purchases, including tax, is $" + totalCost); // prints the total cost with tax

  } // end of main method
} // end of class