// Chiara Hubrich 10/21/18
// CSE 02 EncryptedX HW 6
// Uses the scanner class to get an integer 0-100 from the user (and checks to see that the value is an integer within the range), and then creates
// a square with a hidden X of length and width squareSize based on the value input by the user using nested for-loops

// import scanner class
import java.util.Scanner;
// define a class
public class EncryptedX{
  // main method required for every java program
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in); // construct instance of scanner
    
    int tempVal = 0; // temporary holder for value of squareSize
    int squareSize = 0; // variable for the user input of the size of the square pattern
    String wrongVal = ""; // holds any values the user inputs that are not of the right type
    String enterVal = ""; // holds the value of the enter input when a user types in scanner
    boolean check = false; // the boolean value to check for the correct type in hasNextInt
    
    // while statement to check that the size value is in the range 0-100 and an integer
    System.out.println("Input an integer 0-100 for the square size: ");
    while (squareSize <= 0){
      check = myScanner.hasNextInt();
      if (!check){ // checks if int value
        wrongVal = myScanner.nextLine(); // holds the incorrect input
        System.out.println("Invalid input. Please input an integer 0-100: "); // prints to inform the user their input was wrong
        continue; // restarts the while loop
      }
      else if (check){
        tempVal = myScanner.nextInt();
        if (tempVal > 100 || tempVal < 0){ // checks if in range 0-100
          wrongVal = myScanner.nextLine();
          System.out.println("Invalid input. Please input an integer 0-100: ");
          continue;
        }
        squareSize = tempVal;
        enterVal = myScanner.nextLine();
        break;
      }
    }
    
    for (int numRow = 1; numRow <= (squareSize + 1); numRow++){ // iterates through each row at a time, incrementing up until the user input + 1
      for (int perRow = 1; perRow <= (squareSize + 1); perRow++){ // iterates through each place in the row up until the user input + 1
        if (perRow == numRow || perRow == (squareSize + 2 - numRow) || squareSize == 0){ // outputs a space to create the X
          System.out.print(" ");
        }
        else{
          System.out.print("*");
        }
      }
      System.out.println(); // outputs the new line between each row of the pattern
    }
    
  } // end of main
} // end of class