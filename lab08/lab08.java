// Chiara Hubrich 11/9/18
// CSE 02 lab08 Lab 8
// Create an array with 100 random numbers (values 0-99) and then count and print the number of occurrences for each number

// define a class
public class lab08{
  // main method required for every java program
  public static void main(String[] args){
    
    int[] randVals = new int[100]; // creates an array to hold the 100 random values
    int[] countVals = new int[100]; // creates an array to count the occurrences in randVals
    
    System.out.print("Array 1 holds the following integers: ");
    for(int i = 0; i < randVals.length; i++){
      randVals[i] = (int) (Math.random() * (100)); // assigns a random value (0-99) to each index in randVals
      System.out.print(randVals[i] + " "); // prints the value
    }
    System.out.println();
    System.out.println();
    
    int tempVal = 0; // used to temporarily hold the value held in each index
    for(int i = 0; i < randVals.length; i++){
      tempVal = randVals[i]; // assigned the value in the ith index of randVals
      countVals[tempVal]++; // works as a counter to add 1 to the index of a value every time that value comes up
    }
    
    for(int i = 0; i < countVals.length; i++){
      if(countVals[i] == 1){ // makes it so the "time" in the print statement is not plural for single occurences
        System.out.println(i + " occurs " + countVals[i] + " time");
      }
      else if(countVals[i] > 1){ // excludes 0 occurences from the print values
        System.out.println(i + " occurs " + countVals[i] + " times");
      }
    }
    
  } // end of main
} // end of class