// Chiara Hubrich 9/21/18
// CSE 02 Card Generator Lab 4
// Uses the random class to generate a random integer value in the range of 1-52, then assigns each integer value to a specific suit
// and card identity in a deck of 52 cards using if and switch statements.

//import random class
import java.util.Random;
// define a class
public class CardGenerator{
  // main method required for every java program
  public static void main(String[] args){
    
    Random randGen = new Random(); // new random number generator
    
    int cardInt = randGen.nextInt(52) + 1; // generates a random integer from 1-52
    String suitName; // name of the suit that integer corresponds to
    String cardIdentity; // the identity of that card, such as ace, king, 3, etc.
    
    // uses if and else-if statements to assign the random integer to a suit value based on the range it fits into (1-13, 14-26, 27-39, 40-52)
    if (cardInt >= 1 && cardInt <= 13){
      suitName = " of Diamonds";
    }
    else if (cardInt >= 14 && cardInt <= 26){
      suitName = " of Clubs";
    }
    else if (cardInt >= 27 && cardInt <= 39){
      suitName = " of Hearts";
    }
    else{
      suitName = " of Spades";
    }
    
    int cardVal = cardInt % 13; // uses modulus operator to assign each value to a number 0-12 to eventually get cardIdentity
    
    // uses switch statement to assign the identities of the cards to their value (0-12 mod 13)
    switch (cardVal){
      case 1: cardIdentity = "Ace";
        break;
      case 2: cardIdentity = "2";
        break;
      case 3: cardIdentity = "3";
        break;
      case 4: cardIdentity = "4";
        break;
      case 5: cardIdentity = "5";
        break;
      case 6: cardIdentity = "6";
        break;
      case 7: cardIdentity = "7";
        break;
      case 8: cardIdentity = "8";
        break;
      case 9: cardIdentity = "9";
        break;
      case 10: cardIdentity = "10";
        break;
      case 11: cardIdentity = "Jack";
        break;
      case 12: cardIdentity = "Queen";
        break;
      case 0: cardIdentity = "King";
        break;
      default: cardIdentity = "Invalid";
        break;
    }

    System.out.println("You picked the " + cardIdentity + suitName); // prints the full name of the card
    
  } // end of main
} // end of class